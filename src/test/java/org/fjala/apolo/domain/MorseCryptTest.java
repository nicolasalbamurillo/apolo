package org.fjala.apolo.domain;

import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

class MorseCryptTest {

    @Test
    void encrypt() {
        Crypt crypt = new MorseCrypt();
        String text = "Hello, how are you?";
        String expected = ".... . .-.. .-.. --- --..--   .... --- .--   .- .-. .   -.-- --- ..- ..--..";
        assertThat(crypt.encrypt(text), equalTo(expected));
    }

    @Test
    void decrypt() {
        Crypt crypt = new MorseCrypt();
        String code = ".... . .-.. .-.. --- --..--   .... --- .--   .- .-. .   -.-- --- ..- ..--..";
        String expected = "HELLO, HOW ARE YOU?";
        assertThat(crypt.decrypt(code), equalTo(expected));
    }
}