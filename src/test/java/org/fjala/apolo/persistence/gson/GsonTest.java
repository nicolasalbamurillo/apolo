package org.fjala.apolo.persistence.gson;

import com.google.gson.*;
import org.fjala.apolo.domain.Note;
import org.fjala.apolo.persistence.LocalDateTimeDeserializer;
import org.fjala.apolo.persistence.LocalDateTimeSerializer;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class GsonTest {
    @Test
    void searialized() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(LocalDateTime.class, new LocalDateTimeSerializer());
        gsonBuilder.registerTypeAdapter(LocalDateTime.class, new LocalDateTimeDeserializer());

        Gson gson = gsonBuilder.setPrettyPrinting().create();

        LocalDateTime now =  LocalDateTime.now();
        Note note = new Note("Content", now);
        System.out.println(gson.toJson(note));
        System.out.println(gson.fromJson(gson.toJson(note), Note.class));
    }
}