package org.fjala.apolo.cli;

import org.fjala.apolo.cli.menu.Menu;
import org.fjala.apolo.cli.menu.Option;
import org.fjala.apolo.domain.Note;
import org.fjala.apolo.domain.SimpleNoteService;

import java.util.Collection;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;

public class NoteMenu  extends Menu {
    public NoteMenu(Collection<Note> notes, Consumer<Note> consumer) {
        System.out.println("Select a note: ");
        addOption(0, "Exit", this::exit);
        int i = 0;
        for (Note note : notes) {
            i++;
            addOption(i, note.toString(), () -> {
                consumer.accept(note);
                exit();
            });
        }
    }
}
