package org.fjala.apolo.cli;

import org.fjala.apolo.cli.menu.Menu;
import org.fjala.apolo.domain.Note;
import org.fjala.apolo.domain.SimpleNoteService;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Iterator;

public class MainMenu extends Menu {

    private SimpleNoteService service;

    public MainMenu(SimpleNoteService service) {
        this.service = service;
        addOption(0, "Exit", ()-> System.out.println("Good Bye"));
        addOption(1, "Register a new note", () -> {
            System.out.println("Type content:");
            String content = IN.nextLine();
            Note note = new Note(content, LocalDateTime.now());
            service.registerANote(note);
        });

        addOption(2, "Get Last Three Notes", () -> {
            Collection<Note> notes = service.getLastThreeNotes();
            Iterator<Note> noteIterator = notes.iterator();
            for (int i = 1; i <= notes.size(); i++) {
                System.out.println(i + ":" + noteIterator.next());
            }
        });

        addOption(3, "Encrypt a note", () -> {
            NoteMenu menu = new NoteMenu(service.getAllNotes(), note -> {
                service.encryptNote(note);
                System.out.println("The note was encrypted");
            });
            menu.start();
        });

        addOption(4, "Decrypt a note", () -> {
            NoteMenu menu = new NoteMenu(service.getAllNotes(), note -> {
                service.decryptNote(note);
                System.out.println("The note was decrypted");
            });
            menu.start();
        });

        addOption(5, "Count saved Notes", () -> {
            System.out.println("There are " + service.countSavedNotes() + " saved notes");
        });
    }
}
