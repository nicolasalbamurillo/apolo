package org.fjala.apolo.cli.menu;

import java.util.HashSet;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

public class Menu {
    protected static final int EXIT_OPTION = 0;
    protected static final Scanner IN = new Scanner(System.in);

    private static final String DEFAULT_OPTION_MESSAGE = "Type a option: ";
    private static final String DEFAULT_INVALID_OPTION_MESSAGE = "Invalid option, try again";
    private static final String DEFAULT_ERROR_MESSAGE = "Error: ";

    protected int currentOption;
    protected final Map<Integer, Option> options;
    protected final Map<Integer, String> messages;
    protected final HashSet<Integer> hiddenOptions;

    protected String optionMessage;
    protected String invalidOptionMessage;
    protected String errorMessage;

    public Menu() {
        this(DEFAULT_OPTION_MESSAGE, DEFAULT_INVALID_OPTION_MESSAGE, DEFAULT_ERROR_MESSAGE);
    }

    public Menu(String optionMessage, String invalidOptionMessage, String errorMessage) {
        this.options = new TreeMap<>();
        this.messages = new TreeMap<>();
        this.hiddenOptions =  new HashSet<>();
        this.optionMessage = optionMessage;
        this.errorMessage = errorMessage;
        this.invalidOptionMessage = invalidOptionMessage;
    }

    public void start() {
        do {
            printMenu();
            currentOption = requestOption();
            doOption(currentOption);
            System.out.println();
        } while (currentOption != EXIT_OPTION);
    }

    protected void exit() {
        this.currentOption = EXIT_OPTION;
    }

    private int requestOption() {
        try {
            System.out.print(optionMessage);
            int option = Integer.parseInt(IN.nextLine());

            if(!options.containsKey(option) || hiddenOptions.contains(option)) {
                System.out.println(invalidOptionMessage);
                return requestOption();
            }
            return option;
        } catch (NumberFormatException exception) {
            System.out.println(invalidOptionMessage);
            return requestOption();
        }
    }

    protected void changeMessage(int index, String message) {
        if (messages.containsKey(index)) {
            messages.put(index, message);
        }
    }

    protected void printMenu() {
        messages.forEach((index, message) -> {
            if (!hiddenOptions.contains(index)) {
                System.out.printf("%s. %s\n", index, message);
            }
        });
    }

    public void addOption(int index, String message, Option option) {
        options.put(index, option);
        messages.put(index, message);
    }

    protected void hideOptions(int... options) {
        for (int option : options) {
            hiddenOptions.add(option);
        }
    }

    protected void showOptions(int... options) {
        for (int option : options) {
            hiddenOptions.remove(option);
        }
    }

    public void doOption(int index) {
        try {
            options.get(index).doOption();
        } catch (Exception exception) {
            System.out.println(errorMessage + exception.getMessage());
        }
    }
}
