package org.fjala.apolo.cli.menu;

import java.util.function.Consumer;

public class EnumMenu<T extends Enum<T>> extends Menu {
    
    public EnumMenu(Class<T> type, Consumer<T> consumer) {
        addOption(EXIT_OPTION, "BACK", Option.DO_NOTHING);
        T[] values = type.getEnumConstants();
        int index = 1;
        for (T value : values) {
            addOption(index++, value.name(), () -> consumer.accept(value));
        }
    }
}
