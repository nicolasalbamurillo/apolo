package org.fjala.apolo.cli.menu;

@FunctionalInterface
public interface Option {
    Option DO_NOTHING = () -> {};

    void doOption() throws Exception;
}
