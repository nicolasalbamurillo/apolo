package org.fjala.apolo;

import org.fjala.apolo.cli.MainMenu;
import org.fjala.apolo.domain.Crypt;
import org.fjala.apolo.domain.MorseCrypt;
import org.fjala.apolo.domain.NoteRepository;
import org.fjala.apolo.domain.SimpleNoteService;
import org.fjala.apolo.persistence.LocalNoteRepository;

public class App {

    public static void main(String[] args) {
        NoteRepository repository = new LocalNoteRepository();
        Crypt crypt = new MorseCrypt();
        SimpleNoteService service = new SimpleNoteService(repository, crypt);
        new MainMenu(service).start();
    }
}
