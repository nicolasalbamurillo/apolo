package org.fjala.apolo.domain;

import java.util.HashMap;
import java.util.Map;

public class MorseCrypt implements Crypt {

    private final Map<String, String> toMorse;
    private final Map<String, String> toWord;


    public MorseCrypt() {
        toMorse = new HashMap<>();
        toWord = new HashMap<>();
        addSymbol("A", ".-");
        addSymbol("B", "-...");
        addSymbol("C", "-.-.");
        addSymbol("D", "-..");
        addSymbol("E", ".");
        addSymbol("F", "..-.");
        addSymbol("G", "--.");
        addSymbol("H", "....");
        addSymbol("I", "..");
        addSymbol("J", ".---");
        addSymbol("K", "-.-");
        addSymbol("L", ".-..");
        addSymbol("M", "--");
        addSymbol("N", "-.");
        addSymbol("Ñ", "--.--");
        addSymbol("O", "---");
        addSymbol("P", ".--.");
        addSymbol("Q", "--.-");
        addSymbol("R", ".-.");
        addSymbol("S", "...");
        addSymbol("T", "-");
        addSymbol("U", "..-");
        addSymbol("V", "...-");
        addSymbol("W", ".--");
        addSymbol("X", "-..-");
        addSymbol("Y", "-.--");
        addSymbol("Z", "--..");
        addSymbol("0", "-----");
        addSymbol("1", ".----");
        addSymbol("2", "..---");
        addSymbol("3", "...--");
        addSymbol("4", "....-");
        addSymbol("5", ".....");
        addSymbol("6", "-....");
        addSymbol("7", "--...");
        addSymbol("8", "---..");
        addSymbol("9", "----.");
        addSymbol(".", ".-.-.-");
        addSymbol(",", "--..--");
        addSymbol(":", "---...");
        addSymbol("¿", "..-.-");
        addSymbol("?", "..--..");
        addSymbol("'", ".----.");
        addSymbol("-", "-....-");
        addSymbol("/", "-..-.");
        addSymbol("\"", ".-..-.");
        addSymbol("@", ".--.-.");
        addSymbol("=", "-...-");
        addSymbol("!", "−.−.−−");
        addSymbol("+", ".-.-.");
        addSymbol("(", "-.--.");
        addSymbol(")", "-.--.-");
        toWord.put("", " ");
    }

    private void addSymbol(String character, String code) {
        toMorse.put(character, code);
        toWord.put(code, character);
    }

    @Override
    public String encrypt(String content) {
        StringBuilder builder = new StringBuilder();
        char[] chars = content.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            String c = Character.toString(chars[i]);
            String converted = getMorse(c);
            builder.append(converted);
            if (i != chars.length - 1)
                builder.append(' ');
        }
        return builder.toString();
    }

    private String getMorse(String character) {
        return toMorse.getOrDefault(character.toUpperCase(), character);
    }

    @Override
    public String decrypt(String decrypt) {
        String[] parts = decrypt.split("(\\s\\s)|(\\s)");
        StringBuilder builder = new StringBuilder();
        for (String part : parts) {
            builder.append(toWord.getOrDefault(part, part));
        }
        return builder.toString();
    }
}
