package org.fjala.apolo.domain;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Note {
    private Integer id;
    private String content;
    private final LocalDateTime savedTime;

    public Note(String content, LocalDateTime savedTime) {
        this.content = content;
        this.savedTime = savedTime;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public LocalDateTime getSavedTime() {
        return savedTime;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        sb.append(savedTime.format(formatter));
        sb.append(", ").append(content);
        return sb.toString();
    }
}
