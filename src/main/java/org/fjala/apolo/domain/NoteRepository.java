package org.fjala.apolo.domain;

import java.util.Collection;

public interface NoteRepository {
    void save(Note note);

    Collection<Note> getNotes();

    int count();
}
