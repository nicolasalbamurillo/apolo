package org.fjala.apolo.domain;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;

public class SimpleNoteService {

    private final NoteRepository noteRepository;
    private final Crypt crypt;

    public SimpleNoteService(NoteRepository noteRepository, Crypt crypt) {
        this.noteRepository = noteRepository;
        this.crypt = crypt;
    }

    public void registerANote(Note note) {
        noteRepository.save(note);
    }

    public void encryptNote(Note note) {
        String content = note.getContent();
        note.setContent(crypt.encrypt(content));
        noteRepository.save(note);
    }

    public void decryptNote(Note note) {
        String content = note.getContent();
        note.setContent(crypt.decrypt(content));
        noteRepository.save(note);
    }

    public Collection<Note> getLastThreeNotes() {
        Collection<Note> notes = noteRepository.getNotes();
        LinkedList<Note> result = new LinkedList<>();
        Iterator<Note> iterator = notes.iterator();
        for (int i = 0; i < notes.size() - 3; i++)
            iterator.next();
        iterator.forEachRemaining(result::add);
        return result;
    }

    public Collection<Note> getAllNotes() {
        return noteRepository.getNotes();
    }

    public int countSavedNotes() {
        return noteRepository.count();
    }
}
