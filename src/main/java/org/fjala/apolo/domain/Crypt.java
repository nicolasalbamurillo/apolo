package org.fjala.apolo.domain;

public interface Crypt {
    String encrypt(String content);

    String decrypt(String decrypt);
}
