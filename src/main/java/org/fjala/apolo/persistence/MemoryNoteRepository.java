package org.fjala.apolo.persistence;

import org.fjala.apolo.domain.Note;
import org.fjala.apolo.domain.NoteRepository;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public class MemoryNoteRepository implements NoteRepository {

    private final Map<Integer, Note> notes;
    private int autoIncrementId;

    public MemoryNoteRepository() {
        this.notes = new LinkedHashMap<>();
    }

    @Override
    public void save(Note note) {
        if (note.getId() == null) {
            note.setId(getNextId());
        }
        this.notes.put(note.getId(), note);
    }

    private int getNextId() {
        while (notes.containsKey(autoIncrementId)) {
            autoIncrementId++;
        }
        return autoIncrementId++;
    }

    @Override
    public Collection<Note> getNotes() {
        return notes.values();
    }

    @Override
    public int count() {
        return getNotes().size();
    }
}
