package org.fjala.apolo.persistence;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.fjala.apolo.domain.Note;
import org.fjala.apolo.domain.NoteRepository;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Objects;

public class LocalNoteRepository implements NoteRepository {
    private static final String DEFAULT_PATH = "database";
    private static final String VALUES_FOLDER_NAME = "values";
    private static final String AUTO_INCREMENT_ID_PATH = "id.database";
    private static final int START_INDEX = 0;
    private static final String INITIAL_STRUCTURE_ERROR = "Cannot create initial structure in given path";
    private final Gson gson;

    private final String rootPath;
    private final File valuesDirectory;
    private Path autoIncrementalIdPath;

    public LocalNoteRepository() {
        this(DEFAULT_PATH);
    }

    public LocalNoteRepository(String rootPath) {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(LocalDateTime.class, new LocalDateTimeSerializer());
        gsonBuilder.registerTypeAdapter(LocalDateTime.class, new LocalDateTimeDeserializer());

        this.gson = gsonBuilder.setPrettyPrinting().create();
        this.rootPath = rootPath;
        this.valuesDirectory = new File(rootPath, VALUES_FOLDER_NAME);
        try {
            initializeRootPath();
        } catch (IOException e) {
            throw new IllegalArgumentException(INITIAL_STRUCTURE_ERROR, e);
        }
    }

    private void initializeRootPath() throws IOException {
        File root = new File(rootPath);
        if (!root.exists()) {
            Files.createDirectories(root.toPath());
        }
        if (!valuesDirectory.exists()) {
            Files.createDirectory(valuesDirectory.toPath());
        }
        File autoIncrementalIdFile = new File(root, AUTO_INCREMENT_ID_PATH);
        this.autoIncrementalIdPath = autoIncrementalIdFile.toPath();
        if (!autoIncrementalIdFile.exists()) {
            byte[] toWrite = Integer.toString(START_INDEX).getBytes();
            Files.write(autoIncrementalIdPath, toWrite, StandardOpenOption.CREATE_NEW);
        }
    }

    @Override
    public void save(Note note) {
        if (note.getId() == null) {
            note.setId(getNextId());
        }
        String productFileName = Integer.toString(note.getId());
        File productFile = new File(valuesDirectory, productFileName);
        writePath(productFile.toPath(), gson.toJson(note));
    }

    private int getNextId() {
        try {
            int currentId = Integer.parseInt(Files.readString(autoIncrementalIdPath));
            while (isIdInUse(currentId)) {
                currentId++;
            }
            writePath(autoIncrementalIdPath, Integer.toString(currentId + 1));
            return currentId;
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
    }

    private boolean isIdInUse(int id) {
        File productFile = new File(valuesDirectory, Integer.toString(id));
        return productFile.exists();
    }

    @Override
    public Collection<Note> getNotes() {
        Collection<Note> notes = new LinkedList<>();
        for (File valueFile : Objects.requireNonNull(valuesDirectory.listFiles())) {
            String content = getPathContent(valueFile.toPath());
            notes.add(gson.fromJson(content, Note.class));
        }
        return notes;
    }

    @Override
    public int count() {
        return getNotes().size();
    }

    private String getPathContent(Path path) {
        try {
            return Files.readString(path);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void writePath(Path path, String content) {
        byte[] toWrite = content.getBytes();
        try {
            Files.write(path, toWrite);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
